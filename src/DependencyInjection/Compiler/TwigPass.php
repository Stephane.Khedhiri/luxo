<?php

namespace Luxo\DependencyInjection\Compiler;

use Symfony\Bridge\Twig\Extension\AssetExtension;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Component\Asset\Packages;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Form\FormFactory;

class TwigPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if ($container->has(Packages::class)) {
            $container
                ->register(AssetExtension::class, AssetExtension::class)
                ->setArguments([
                    new Reference(Packages::class),
                ])
                ->addTag('twig.extension')
            ;
        }

        if ($container->has(FormFactory::class)) {
            $container
                ->register(FormExtension::class, FormExtension::class)
                ->addTag('twig.extension')
            ;
        }

        foreach ($container->findTaggedServiceIds('twig.extension') as $extension => $tags) {
            $container->getDefinition(\Twig\Environment::class)->addMethodCall('addExtension', [new Reference($extension)]);
        }
    }
}
