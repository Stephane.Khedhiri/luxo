<?php

namespace Luxo\Command\Doctrine;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixtureLoadCommand extends Command
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
      ->setName('doctrine:fixture:load')
    ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $loader = new \Nelmio\Alice\Loader\NativeLoader();
        $objectSet = $loader->loadData([
      \Luxo\Entity\Announcement::class => [
        'announcement_{1..10}' => [
          'title' => '<username()>',
          'description' => '<text()>',
        ],
      ],
    ]);

        foreach ($objectSet->getObjects() as $item) {
            $this->entityManager->persist($item);
        }

        $this->entityManager->flush();

        return 0;
    }
}
