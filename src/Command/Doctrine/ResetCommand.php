<?php

namespace Luxo\Command\Doctrine;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ResetCommand extends Command
{
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $dropCommand = $this->getApplication()->find('doctrine:database:drop');

        $dropCommandReturnCode = $dropCommand->run(new ArrayInput([
      'command' => $dropCommand->getName(),
      '--if-exists' => true,
      '--force' => true,
    ]), $output);

        $createCommand = $this->getApplication()->find('doctrine:database:create');

        $createCommandReturnCode = $createCommand->run(new ArrayInput([
      'command' => $createCommand->getName(),
      '--if-not-exists' => true,
    ]), $output);

        $schemaCreateCommand = $this->getApplication()->find('doctrine:schema:create');

        $schemaCreateCommandReturnCode = $schemaCreateCommand->run(new ArrayInput([
      'command' => $schemaCreateCommand->getName(),
    ]), $output);

        $fixtureLoadCommand = $this->getApplication()->find('doctrine:fixture:load');

        $fixtureLoadCommandReturnCode = $fixtureLoadCommand->run(new ArrayInput([
      'command' => $fixtureLoadCommand->getName(),
    ]), $output);

        return 0;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('doctrine:database:reset')
        ;
    }
}
