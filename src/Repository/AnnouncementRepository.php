<?php

namespace Luxo\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Luxo\Entity\Announcement;

class AnnouncementRepository extends EntityRepository
{
    public function __construct(EntityManager $em)
    {
        parent::__construct($em, $em->getClassMetadata(Announcement::class));
    }

    public function findAllWithImages()
    {
        $queryBuilder = $this->createQueryBuilder('a');
        $query = $queryBuilder
            ->getQuery();

        return $query->getResult();
    }
}
