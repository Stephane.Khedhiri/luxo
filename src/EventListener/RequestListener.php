<?php

namespace Luxo\EventListener;

use Luxo\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class RequestListener implements EventSubscriberInterface
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function onKernelRequest(RequestEvent $requestEvent)
    {
        $this->requestStack->push($requestEvent->getRequest());
    }

    public static function getSubscribedEvents()
    {
        return [
          RequestEvent::NAME => [
              ['onKernelRequest', -99],
          ],
        ];
    }
}
