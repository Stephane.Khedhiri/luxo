<?php

namespace Luxo\EventListener;

use Luxo\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;

class SessionListener implements EventSubscriberInterface
{
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var Session
     */
    private $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function onKernelRequest(RequestEvent $requestEvent)
    {
        $this->session->start();
    }

    public static function getSubscribedEvents()
    {
        return [
          RequestEvent::NAME => [
              ['onKernelRequest', -80],
          ],
        ];
    }
}
