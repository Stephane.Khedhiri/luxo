<?php

namespace Luxo\Action\Login;

use Luxo\Action\Action;

class ShowAction extends Action
{
    public function __invoke()
    {
        return $this->render('login');
    }
}
