<?php

namespace Luxo\Action\Register;

use Luxo\Action\Action;

class ShowAction extends Action
{
    public function __invoke()
    {
        return $this->render('register');
    }
}
