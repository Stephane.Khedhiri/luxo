<?php

namespace Luxo\Action;

use Symfony\Component\HttpFoundation\Response;

abstract class Action
{
    /**
     * @var \Twig\Environment
     */
    private $twig;

    public function __construct(\Twig\Environment $twig = null)
    {
        $this->twig = $twig;
    }

    /**
     * @param $template
     * @param array $context
     *
     * @return string
     */
    public function renderTemplate($template, $context = [])
    {
        return $this->twig->render($template, $context);
    }

    /**
     * @param $template
     * @param $context
     *
     * @return Response
     */
    public function render($template, $context = [])
    {
        $response = new Response();
        $response->setContent($this->renderTemplate($template, $context));

        return $response;
    }
}
