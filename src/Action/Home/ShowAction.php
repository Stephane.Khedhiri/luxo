<?php

namespace Luxo\Action\Home;

use Doctrine\ORM\EntityManager;
use Luxo\Action\Action;
use Luxo\Repository\AnnouncementRepository;
use Symfony\Component\Routing\Annotation\Route;

class ShowAction extends Action
{
    /**
     * @Route(path="/")
     *
     * @param EntityManager $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function __invoke(AnnouncementRepository $announcementRepository)
    {
        return $this->render('home.html.twig');
    }
}
